from .....Internal.Core import Core
from .....Internal.CommandsGroup import CommandsGroup
from .....Internal import Conversions
from .....Internal.Types import DataType
from .....Internal.ArgSingleList import ArgSingleList
from .....Internal.ArgSingle import ArgSingle
from ..... import enums


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class CountCls:
	"""Count commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("count", core, parent)

	def get(self, filter_py: str = None, mode: enums.ExpressionMode = None) -> int:
		"""SCPI: STATus:EVENt:BITS:COUNt \n
		Snippet: value: int = driver.status.event.bits.count.get(filter_py = 'abc', mode = enums.ExpressionMode.REGex) \n
		Returns the number of events listed by method RsCmwBase.Status.Event.Bits.All.get_. \n
			:param filter_py: No help available
			:param mode: No help available
			:return: count: decimal"""
		param = ArgSingleList().compose_cmd_string(ArgSingle('filter_py', filter_py, DataType.String, None, is_optional=True), ArgSingle('mode', mode, DataType.Enum, enums.ExpressionMode, is_optional=True))
		response = self._core.io.query_str(f'STATus:EVENt:BITS:COUNt? {param}'.rstrip())
		return Conversions.str_to_int(response)
