from .....Internal.Core import Core
from .....Internal.CommandsGroup import CommandsGroup
from .....Internal.RepeatedCapability import RepeatedCapability
from ..... import repcap


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class RsibCls:
	"""Rsib commands group definition. 1 total commands, 1 Subgroups, 0 group commands
	Repeated Capability: RsibInstance, default value after init: RsibInstance.Inst1"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("rsib", core, parent)
		self._cmd_group.rep_cap = RepeatedCapability(self._cmd_group.group_name, 'repcap_rsibInstance_get', 'repcap_rsibInstance_set', repcap.RsibInstance.Inst1)

	def repcap_rsibInstance_set(self, rsibInstance: repcap.RsibInstance) -> None:
		"""Repeated Capability default value numeric suffix.
		This value is used, if you do not explicitely set it in the child set/get methods, or if you leave it to RsibInstance.Default
		Default value after init: RsibInstance.Inst1"""
		self._cmd_group.set_repcap_enum_value(rsibInstance)

	def repcap_rsibInstance_get(self) -> repcap.RsibInstance:
		"""Returns the current default repeated capability for the child set/get methods"""
		# noinspection PyTypeChecker
		return self._cmd_group.get_repcap_enum_value()

	@property
	def vresource(self):
		"""vresource commands group. 0 Sub-classes, 1 commands."""
		if not hasattr(self, '_vresource'):
			from .Vresource import VresourceCls
			self._vresource = VresourceCls(self._core, self._cmd_group)
		return self._vresource

	def clone(self) -> 'RsibCls':
		"""Clones the group by creating new object from it and its whole existing subgroups
		Also copies all the existing default Repeated Capabilities setting,
		which you can change independently without affecting the original group"""
		new_group = RsibCls(self._core, self._cmd_group.parent)
		self._cmd_group.synchronize_repcaps(new_group)
		return new_group
