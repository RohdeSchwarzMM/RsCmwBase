from ....Internal.Core import Core
from ....Internal.CommandsGroup import CommandsGroup


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class SingleCmwCls:
	"""SingleCmw commands group definition. 8 total commands, 1 Subgroups, 0 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("singleCmw", core, parent)

	@property
	def freqCorrection(self):
		"""freqCorrection commands group. 3 Sub-classes, 1 commands."""
		if not hasattr(self, '_freqCorrection'):
			from .FreqCorrection import FreqCorrectionCls
			self._freqCorrection = FreqCorrectionCls(self._core, self._cmd_group)
		return self._freqCorrection

	def clone(self) -> 'SingleCmwCls':
		"""Clones the group by creating new object from it and its whole existing subgroups
		Also copies all the existing default Repeated Capabilities setting,
		which you can change independently without affecting the original group"""
		new_group = SingleCmwCls(self._core, self._cmd_group.parent)
		self._cmd_group.synchronize_repcaps(new_group)
		return new_group
