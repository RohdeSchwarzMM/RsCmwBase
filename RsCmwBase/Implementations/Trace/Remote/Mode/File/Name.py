from ......Internal.Core import Core
from ......Internal.CommandsGroup import CommandsGroup
from ......Internal import Conversions
from ......Internal.Utilities import trim_str_response
from ...... import repcap


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class NameCls:
	"""Name commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("name", core, parent)

	def set(self, bs_file_path: str, fileNr=repcap.FileNr.Default) -> None:
		"""SCPI: TRACe:REMote:MODE:FILE<instrument>:NAME \n
		Snippet: driver.trace.remote.mode.file.name.set(bs_file_path = 'abc', fileNr = repcap.FileNr.Default) \n
		Specify path and name of the target file for tracing of the remote interface. For different subinstruments, specify
		different files. If you specify a new target file while tracing, the old target file is closed, the new file is created
		and tracing is continued with the new file. \n
			:param bs_file_path: No help available
			:param fileNr: optional repeated capability selector. Default value: Nr1 (settable in the interface 'File')
		"""
		param = Conversions.value_to_quoted_str(bs_file_path)
		fileNr_cmd_val = self._cmd_group.get_repcap_cmd_value(fileNr, repcap.FileNr)
		self._core.io.write(f'TRACe:REMote:MODE:FILE{fileNr_cmd_val}:NAME {param}')

	def get(self, fileNr=repcap.FileNr.Default) -> str:
		"""SCPI: TRACe:REMote:MODE:FILE<instrument>:NAME \n
		Snippet: value: str = driver.trace.remote.mode.file.name.get(fileNr = repcap.FileNr.Default) \n
		Specify path and name of the target file for tracing of the remote interface. For different subinstruments, specify
		different files. If you specify a new target file while tracing, the old target file is closed, the new file is created
		and tracing is continued with the new file. \n
			:param fileNr: optional repeated capability selector. Default value: Nr1 (settable in the interface 'File')
			:return: bs_file_path: No help available"""
		fileNr_cmd_val = self._cmd_group.get_repcap_cmd_value(fileNr, repcap.FileNr)
		response = self._core.io.query_str(f'TRACe:REMote:MODE:FILE{fileNr_cmd_val}:NAME?')
		return trim_str_response(response)
