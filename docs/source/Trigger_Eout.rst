Eout<Eout>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr4
	rc = driver.trigger.eout.repcap_eout_get()
	driver.trigger.eout.repcap_eout_set(repcap.Eout.Nr1)





.. autoclass:: RsCmwBase.Implementations.Trigger.Eout.EoutCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.eout.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Eout_Catalog.rst
	Trigger_Eout_Source.rst