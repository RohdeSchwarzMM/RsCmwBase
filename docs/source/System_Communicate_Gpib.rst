Gpib<GpibInstance>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Inst1 .. Inst32
	rc = driver.system.communicate.gpib.repcap_gpibInstance_get()
	driver.system.communicate.gpib.repcap_gpibInstance_set(repcap.GpibInstance.Inst1)





.. autoclass:: RsCmwBase.Implementations.System.Communicate.Gpib.GpibCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.system.communicate.gpib.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	System_Communicate_Gpib_Self.rst
	System_Communicate_Gpib_Vresource.rst