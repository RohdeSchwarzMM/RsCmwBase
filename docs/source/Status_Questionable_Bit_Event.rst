Event
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: STATus:QUEStionable:BIT<bitno>[:EVENt]

.. code-block:: python

	STATus:QUEStionable:BIT<bitno>[:EVENt]



.. autoclass:: RsCmwBase.Implementations.Status.Questionable.Bit.Event.EventCls
	:members:
	:undoc-members:
	:noindex: