Size
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRACe:REMote:MODE:FILE<instrument>:SIZE

.. code-block:: python

	TRACe:REMote:MODE:FILE<instrument>:SIZE



.. autoclass:: RsCmwBase.Implementations.Trace.Remote.Mode.File.Size.SizeCls
	:members:
	:undoc-members:
	:noindex: