Procedure
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: PROCedure:CMWD

.. code-block:: python

	PROCedure:CMWD



.. autoclass:: RsCmwBase.Implementations.Procedure.ProcedureCls
	:members:
	:undoc-members:
	:noindex: