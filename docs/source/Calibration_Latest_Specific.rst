Specific
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CALibration:BASE:LATest:SPECific

.. code-block:: python

	CALibration:BASE:LATest:SPECific



.. autoclass:: RsCmwBase.Implementations.Calibration.Latest.Specific.SpecificCls
	:members:
	:undoc-members:
	:noindex: