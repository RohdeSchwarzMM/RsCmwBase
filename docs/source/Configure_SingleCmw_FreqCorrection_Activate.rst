Activate
----------------------------------------





.. autoclass:: RsCmwBase.Implementations.Configure.SingleCmw.FreqCorrection.Activate.ActivateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.singleCmw.freqCorrection.activate.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_SingleCmw_FreqCorrection_Activate_Rx.rst
	Configure_SingleCmw_FreqCorrection_Activate_Tx.rst