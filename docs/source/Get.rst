Get
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: GET:XVALues

.. code-block:: python

	GET:XVALues



.. autoclass:: RsCmwBase.Implementations.Get.GetCls
	:members:
	:undoc-members:
	:noindex: