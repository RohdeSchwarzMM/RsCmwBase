Operation
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: STATus:OPERation[:EVENt]
	single: STATus:OPERation:CONDition
	single: STATus:OPERation:ENABle
	single: STATus:OPERation:PTRansition
	single: STATus:OPERation:NTRansition

.. code-block:: python

	STATus:OPERation[:EVENt]
	STATus:OPERation:CONDition
	STATus:OPERation:ENABle
	STATus:OPERation:PTRansition
	STATus:OPERation:NTRansition



.. autoclass:: RsCmwBase.Implementations.Status.Operation.OperationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.status.operation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Status_Operation_Bit.rst