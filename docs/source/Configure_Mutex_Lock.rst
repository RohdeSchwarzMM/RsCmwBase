Lock
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:MUTex:LOCK

.. code-block:: python

	CONFigure:MUTex:LOCK



.. autoclass:: RsCmwBase.Implementations.Configure.Mutex.Lock.LockCls
	:members:
	:undoc-members:
	:noindex: