License
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:BASE:DEVice:LICense

.. code-block:: python

	SYSTem:BASE:DEVice:LICense



.. autoclass:: RsCmwBase.Implementations.System.Device.License.LicenseCls
	:members:
	:undoc-members:
	:noindex: