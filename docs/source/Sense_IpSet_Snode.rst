Snode
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SENSe:BASE:IPSet:SNODe:NNAMe
	single: SENSe:BASE:IPSet:SNODe:NTYPe
	single: SENSe:BASE:IPSet:SNODe:NSEGment

.. code-block:: python

	SENSe:BASE:IPSet:SNODe:NNAMe
	SENSe:BASE:IPSet:SNODe:NTYPe
	SENSe:BASE:IPSet:SNODe:NSEGment



.. autoclass:: RsCmwBase.Implementations.Sense.IpSet.Snode.SnodeCls
	:members:
	:undoc-members:
	:noindex: