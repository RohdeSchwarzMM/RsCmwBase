Version
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:BASE:OPTion:VERSion

.. code-block:: python

	SYSTem:BASE:OPTion:VERSion



.. autoclass:: RsCmwBase.Implementations.System.Option.Version.VersionCls
	:members:
	:undoc-members:
	:noindex: