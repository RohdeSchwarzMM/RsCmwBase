Bit<BitNr>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr8 .. Nr12
	rc = driver.status.questionable.bit.repcap_bitNr_get()
	driver.status.questionable.bit.repcap_bitNr_set(repcap.BitNr.Nr8)





.. autoclass:: RsCmwBase.Implementations.Status.Questionable.Bit.BitCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.status.questionable.bit.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Status_Questionable_Bit_Condition.rst
	Status_Questionable_Bit_Enable.rst
	Status_Questionable_Bit_Event.rst
	Status_Questionable_Bit_Ntransition.rst
	Status_Questionable_Bit_Ptransition.rst