MacroCreate
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: *DMC

.. code-block:: python

	*DMC



.. autoclass:: RsCmwBase.Implementations.MacroCreate.MacroCreateCls
	:members:
	:undoc-members:
	:noindex: