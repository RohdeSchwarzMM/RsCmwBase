Enable
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: STATus:QUEStionable:BIT<bitno>:ENABle

.. code-block:: python

	STATus:QUEStionable:BIT<bitno>:ENABle



.. autoclass:: RsCmwBase.Implementations.Status.Questionable.Bit.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex: