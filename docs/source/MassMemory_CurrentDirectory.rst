CurrentDirectory
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: MMEMory:CDIRectory

.. code-block:: python

	MMEMory:CDIRectory



.. autoclass:: RsCmwBase.Implementations.MassMemory.CurrentDirectory.CurrentDirectoryCls
	:members:
	:undoc-members:
	:noindex: