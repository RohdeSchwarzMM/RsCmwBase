Status
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: STATus:PRESet

.. code-block:: python

	STATus:PRESet



.. autoclass:: RsCmwBase.Implementations.Status.StatusCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.status.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Status_Condition.rst
	Status_Event.rst
	Status_Generator.rst
	Status_Measurement.rst
	Status_Operation.rst
	Status_Questionable.rst
	Status_Queue.rst