Data
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: WRITe:EEPRom:DATA

.. code-block:: python

	WRITe:EEPRom:DATA



.. autoclass:: RsCmwBase.Implementations.Write.Eeprom.Data.DataCls
	:members:
	:undoc-members:
	:noindex: