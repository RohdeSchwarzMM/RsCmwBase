Slot<Slot>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr4
	rc = driver.configure.correction.ifEqualizer.slot.repcap_slot_get()
	driver.configure.correction.ifEqualizer.slot.repcap_slot_set(repcap.Slot.Nr1)





.. autoclass:: RsCmwBase.Implementations.Configure.Correction.IfEqualizer.Slot.SlotCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.correction.ifEqualizer.slot.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Correction_IfEqualizer_Slot_RxFilter.rst
	Configure_Correction_IfEqualizer_Slot_TxFilter.rst