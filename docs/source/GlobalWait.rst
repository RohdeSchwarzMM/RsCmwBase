GlobalWait
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: *GWAI

.. code-block:: python

	*GWAI



.. autoclass:: RsCmwBase.Implementations.GlobalWait.GlobalWaitCls
	:members:
	:undoc-members:
	:noindex: