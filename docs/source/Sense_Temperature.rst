Temperature
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:BASE:TEMPerature:ENVironment

.. code-block:: python

	SENSe:BASE:TEMPerature:ENVironment



.. autoclass:: RsCmwBase.Implementations.Sense.Temperature.TemperatureCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.temperature.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Temperature_Exceeded.rst
	Sense_Temperature_Operating.rst