TxFilter<TxFilter>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr10
	rc = driver.correction.ifEqualizer.trace.magnitude.uncorrected.slot.txFilter.repcap_txFilter_get()
	driver.correction.ifEqualizer.trace.magnitude.uncorrected.slot.txFilter.repcap_txFilter_set(repcap.TxFilter.Nr1)



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BASE:CORRection:IFEQualizer:TRACe:MAGNitude:UNCorrected:SLOT<Slot>:TXFilter<Filter>

.. code-block:: python

	FETCh:BASE:CORRection:IFEQualizer:TRACe:MAGNitude:UNCorrected:SLOT<Slot>:TXFilter<Filter>



.. autoclass:: RsCmwBase.Implementations.Correction.IfEqualizer.Trace.Magnitude.Uncorrected.Slot.TxFilter.TxFilterCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.correction.ifEqualizer.trace.magnitude.uncorrected.slot.txFilter.clone()