Record
----------------------------------------





.. autoclass:: RsCmwBase.Implementations.Diagnostic.Record.RecordCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.diagnostic.record.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Diagnostic_Record_Macro.rst