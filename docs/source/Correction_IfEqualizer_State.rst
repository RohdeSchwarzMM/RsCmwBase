State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BASE:CORRection:IFEQualizer:STATe

.. code-block:: python

	FETCh:BASE:CORRection:IFEQualizer:STATe



.. autoclass:: RsCmwBase.Implementations.Correction.IfEqualizer.State.StateCls
	:members:
	:undoc-members:
	:noindex: