Syntax
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: DIAGnostic:HELP:SYNTax
	single: DIAGnostic:HELP:SYNTax:ALL

.. code-block:: python

	DIAGnostic:HELP:SYNTax
	DIAGnostic:HELP:SYNTax:ALL



.. autoclass:: RsCmwBase.Implementations.Diagnostic.Help.Syntax.SyntaxCls
	:members:
	:undoc-members:
	:noindex: