Utc
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:TIME:UTC

.. code-block:: python

	SYSTem:TIME:UTC



.. autoclass:: RsCmwBase.Implementations.System.Time.Utc.UtcCls
	:members:
	:undoc-members:
	:noindex: