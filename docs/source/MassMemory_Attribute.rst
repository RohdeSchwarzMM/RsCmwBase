Attribute
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: MMEMory:ATTRibute

.. code-block:: python

	MMEMory:ATTRibute



.. autoclass:: RsCmwBase.Implementations.MassMemory.Attribute.AttributeCls
	:members:
	:undoc-members:
	:noindex: