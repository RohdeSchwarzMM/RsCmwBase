State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:CMWD:STATe

.. code-block:: python

	FETCh:CMWD:STATe



.. autoclass:: RsCmwBase.Implementations.Cmwd.State.StateCls
	:members:
	:undoc-members:
	:noindex: