Rewait
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:SPOint:REWait

.. code-block:: python

	CONFigure:SPOint:REWait



.. autoclass:: RsCmwBase.Implementations.Configure.Spoint.Rewait.RewaitCls
	:members:
	:undoc-members:
	:noindex: