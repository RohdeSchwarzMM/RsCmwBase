Macro
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: MMEMory:STORe:MACRo

.. code-block:: python

	MMEMory:STORe:MACRo



.. autoclass:: RsCmwBase.Implementations.MassMemory.Store.Macro.MacroCls
	:members:
	:undoc-members:
	:noindex: