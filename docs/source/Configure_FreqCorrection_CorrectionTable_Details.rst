Details
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BASE:FDCorrection:CTABle:DETails

.. code-block:: python

	CONFigure:BASE:FDCorrection:CTABle:DETails



.. autoclass:: RsCmwBase.Implementations.Configure.FreqCorrection.CorrectionTable.Details.DetailsCls
	:members:
	:undoc-members:
	:noindex: