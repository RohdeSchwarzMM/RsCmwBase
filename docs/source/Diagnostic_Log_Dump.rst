Dump
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:LOG:DUMP

.. code-block:: python

	DIAGnostic:LOG:DUMP



.. autoclass:: RsCmwBase.Implementations.Diagnostic.Log.Dump.DumpCls
	:members:
	:undoc-members:
	:noindex: