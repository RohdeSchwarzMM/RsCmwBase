Parser
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRACe:REMote:MODE:FILE<instrument>:PARSer

.. code-block:: python

	TRACe:REMote:MODE:FILE<instrument>:PARSer



.. autoclass:: RsCmwBase.Implementations.Trace.Remote.Mode.File.Parser.ParserCls
	:members:
	:undoc-members:
	:noindex: