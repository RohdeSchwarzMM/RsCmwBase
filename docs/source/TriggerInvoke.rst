TriggerInvoke
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: *TRG

.. code-block:: python

	*TRG



.. autoclass:: RsCmwBase.Implementations.TriggerInvoke.TriggerInvokeCls
	:members:
	:undoc-members:
	:noindex: