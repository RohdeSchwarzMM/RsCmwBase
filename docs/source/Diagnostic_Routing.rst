Routing
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:ROUTing:CATalog

.. code-block:: python

	DIAGnostic:ROUTing:CATalog



.. autoclass:: RsCmwBase.Implementations.Diagnostic.Routing.RoutingCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.diagnostic.routing.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Diagnostic_Routing_Expert.rst