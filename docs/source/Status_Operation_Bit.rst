Bit<BitNr>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr8 .. Nr12
	rc = driver.status.operation.bit.repcap_bitNr_get()
	driver.status.operation.bit.repcap_bitNr_set(repcap.BitNr.Nr8)





.. autoclass:: RsCmwBase.Implementations.Status.Operation.Bit.BitCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.status.operation.bit.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Status_Operation_Bit_Condition.rst
	Status_Operation_Bit_Enable.rst
	Status_Operation_Bit_Event.rst
	Status_Operation_Bit_Ntransition.rst
	Status_Operation_Bit_Ptransition.rst