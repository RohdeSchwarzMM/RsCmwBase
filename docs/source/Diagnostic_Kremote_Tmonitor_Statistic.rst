Statistic
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:KREMote:TMONitor:STATistic

.. code-block:: python

	DIAGnostic:KREMote:TMONitor:STATistic



.. autoclass:: RsCmwBase.Implementations.Diagnostic.Kremote.Tmonitor.Statistic.StatisticCls
	:members:
	:undoc-members:
	:noindex: