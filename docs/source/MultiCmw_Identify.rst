Identify
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: STARt:BASE:MCMW:IDENtify

.. code-block:: python

	STARt:BASE:MCMW:IDENtify



.. autoclass:: RsCmwBase.Implementations.MultiCmw.Identify.IdentifyCls
	:members:
	:undoc-members:
	:noindex: