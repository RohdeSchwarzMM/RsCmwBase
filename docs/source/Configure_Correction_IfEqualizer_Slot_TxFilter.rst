TxFilter
----------------------------------------





.. autoclass:: RsCmwBase.Implementations.Configure.Correction.IfEqualizer.Slot.TxFilter.TxFilterCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.correction.ifEqualizer.slot.txFilter.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Correction_IfEqualizer_Slot_TxFilter_Select.rst