Exist
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BASE:FDCorrection:CTABle:EXISt

.. code-block:: python

	CONFigure:BASE:FDCorrection:CTABle:EXISt



.. autoclass:: RsCmwBase.Implementations.Configure.FreqCorrection.CorrectionTable.Exist.ExistCls
	:members:
	:undoc-members:
	:noindex: