Rx
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:CMWS:FDCorrection:ACTivate:RX

.. code-block:: python

	CONFigure:CMWS:FDCorrection:ACTivate:RX



.. autoclass:: RsCmwBase.Implementations.Configure.SingleCmw.FreqCorrection.Activate.Rx.RxCls
	:members:
	:undoc-members:
	:noindex: