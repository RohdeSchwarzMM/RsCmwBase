Condition
----------------------------------------





.. autoclass:: RsCmwBase.Implementations.Status.Generator.Condition.ConditionCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.status.generator.condition.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Status_Generator_Condition_Off.rst
	Status_Generator_Condition_On.rst
	Status_Generator_Condition_Pending.rst