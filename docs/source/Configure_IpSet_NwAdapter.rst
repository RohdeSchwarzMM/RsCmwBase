NwAdapter<NwAdapter>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Adapter1 .. Adapter5
	rc = driver.configure.ipSet.nwAdapter.repcap_nwAdapter_get()
	driver.configure.ipSet.nwAdapter.repcap_nwAdapter_set(repcap.NwAdapter.Adapter1)



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BASE:IPSet:NWADapter<n>

.. code-block:: python

	CONFigure:BASE:IPSet:NWADapter<n>



.. autoclass:: RsCmwBase.Implementations.Configure.IpSet.NwAdapter.NwAdapterCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.ipSet.nwAdapter.clone()