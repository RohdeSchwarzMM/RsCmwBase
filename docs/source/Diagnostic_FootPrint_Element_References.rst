References
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:FOOTprint:ELEMent:REFerences

.. code-block:: python

	DIAGnostic:FOOTprint:ELEMent:REFerences



.. autoclass:: RsCmwBase.Implementations.Diagnostic.FootPrint.Element.References.ReferencesCls
	:members:
	:undoc-members:
	:noindex: