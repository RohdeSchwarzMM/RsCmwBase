Next
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: STATus:EVENt:BITS:NEXT

.. code-block:: python

	STATus:EVENt:BITS:NEXT



.. autoclass:: RsCmwBase.Implementations.Status.Event.Bits.Next.NextCls
	:members:
	:undoc-members:
	:noindex: