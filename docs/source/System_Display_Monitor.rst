Monitor
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:DISPlay:MONitor

.. code-block:: python

	SYSTem:DISPlay:MONitor



.. autoclass:: RsCmwBase.Implementations.System.Display.Monitor.MonitorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.system.display.monitor.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	System_Display_Monitor_Off.rst