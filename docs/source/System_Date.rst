Date
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:DATE

.. code-block:: python

	SYSTem:DATE



.. autoclass:: RsCmwBase.Implementations.System.Date.DateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.system.date.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	System_Date_Local.rst
	System_Date_Utc.rst