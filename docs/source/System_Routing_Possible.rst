Possible
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:ROUTing:POSSible

.. code-block:: python

	SYSTem:ROUTing:POSSible



.. autoclass:: RsCmwBase.Implementations.System.Routing.Possible.PossibleCls
	:members:
	:undoc-members:
	:noindex: