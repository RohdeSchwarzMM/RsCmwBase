Reference
----------------------------------------





.. autoclass:: RsCmwBase.Implementations.System.Reference.ReferenceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.system.reference.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	System_Reference_Dc.rst
	System_Reference_Frequency.rst
	System_Reference_Phase.rst