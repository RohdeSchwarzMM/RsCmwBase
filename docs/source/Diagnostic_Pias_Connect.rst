Connect
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:PIAS:CONNect

.. code-block:: python

	DIAGnostic:PIAS:CONNect



.. autoclass:: RsCmwBase.Implementations.Diagnostic.Pias.Connect.ConnectCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.diagnostic.pias.connect.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Diagnostic_Pias_Connect_Multiple.rst