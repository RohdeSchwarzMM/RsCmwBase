Duration
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRACe:REMote:MODE:FILE<instrument>:DEXecution:DURation

.. code-block:: python

	TRACe:REMote:MODE:FILE<instrument>:DEXecution:DURation



.. autoclass:: RsCmwBase.Implementations.Trace.Remote.Mode.File.Dexecution.Duration.DurationCls
	:members:
	:undoc-members:
	:noindex: