Run
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: STATus:MEASurement:CONDition:RUN

.. code-block:: python

	STATus:MEASurement:CONDition:RUN



.. autoclass:: RsCmwBase.Implementations.Status.Measurement.Condition.Run.RunCls
	:members:
	:undoc-members:
	:noindex: