Cmw<CmwVariant>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Cmw1 .. Cmw100
	rc = driver.system.cmw.repcap_cmwVariant_get()
	driver.system.cmw.repcap_cmwVariant_set(repcap.CmwVariant.Cmw1)





.. autoclass:: RsCmwBase.Implementations.System.Cmw.CmwCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.system.cmw.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	System_Cmw_Device.rst