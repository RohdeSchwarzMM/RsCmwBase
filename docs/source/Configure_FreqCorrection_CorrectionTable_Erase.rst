Erase
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BASE:FDCorrection:CTABle:ERASe

.. code-block:: python

	CONFigure:BASE:FDCorrection:CTABle:ERASe



.. autoclass:: RsCmwBase.Implementations.Configure.FreqCorrection.CorrectionTable.Erase.EraseCls
	:members:
	:undoc-members:
	:noindex: