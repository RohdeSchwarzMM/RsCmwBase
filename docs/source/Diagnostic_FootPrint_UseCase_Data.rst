Data
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:FOOTprint:USECase:DATA

.. code-block:: python

	DIAGnostic:FOOTprint:USECase:DATA



.. autoclass:: RsCmwBase.Implementations.Diagnostic.FootPrint.UseCase.Data.DataCls
	:members:
	:undoc-members:
	:noindex: