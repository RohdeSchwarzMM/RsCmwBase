Trace
----------------------------------------





.. autoclass:: RsCmwBase.Implementations.Correction.IfEqualizer.Trace.TraceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.correction.ifEqualizer.trace.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Correction_IfEqualizer_Trace_Gdelay.rst
	Correction_IfEqualizer_Trace_Magnitude.rst