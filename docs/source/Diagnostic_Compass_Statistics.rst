Statistics
----------------------------------------





.. autoclass:: RsCmwBase.Implementations.Diagnostic.Compass.Statistics.StatisticsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.diagnostic.compass.statistics.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Diagnostic_Compass_Statistics_Process.rst