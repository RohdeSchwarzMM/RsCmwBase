Off
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: STATus:GENerator:CONDition:OFF

.. code-block:: python

	STATus:GENerator:CONDition:OFF



.. autoclass:: RsCmwBase.Implementations.Status.Generator.Condition.Off.OffCls
	:members:
	:undoc-members:
	:noindex: