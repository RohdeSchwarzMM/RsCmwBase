Source
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:BASE:EOUT<n>:SOURce

.. code-block:: python

	TRIGger:BASE:EOUT<n>:SOURce



.. autoclass:: RsCmwBase.Implementations.Trigger.Eout.Source.SourceCls
	:members:
	:undoc-members:
	:noindex: