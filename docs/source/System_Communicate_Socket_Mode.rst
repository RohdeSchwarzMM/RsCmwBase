Mode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:COMMunicate:SOCKet<inst>:MODE

.. code-block:: python

	SYSTem:COMMunicate:SOCKet<inst>:MODE



.. autoclass:: RsCmwBase.Implementations.System.Communicate.Socket.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex: