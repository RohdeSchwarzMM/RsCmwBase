StartMode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRACe:REMote:MODE:FILE<instrument>:STARtmode

.. code-block:: python

	TRACe:REMote:MODE:FILE<instrument>:STARtmode



.. autoclass:: RsCmwBase.Implementations.Trace.Remote.Mode.File.StartMode.StartModeCls
	:members:
	:undoc-members:
	:noindex: