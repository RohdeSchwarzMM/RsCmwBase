Identify
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BASE:MCMW:IDENtify:BTIMe

.. code-block:: python

	CONFigure:BASE:MCMW:IDENtify:BTIMe



.. autoclass:: RsCmwBase.Implementations.Configure.MultiCmw.Identify.IdentifyCls
	:members:
	:undoc-members:
	:noindex: