Device
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: HCOPy:DEVice:FORMat

.. code-block:: python

	HCOPy:DEVice:FORMat



.. autoclass:: RsCmwBase.Implementations.HardCopy.Device.DeviceCls
	:members:
	:undoc-members:
	:noindex: