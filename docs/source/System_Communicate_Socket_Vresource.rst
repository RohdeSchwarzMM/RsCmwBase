Vresource
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:COMMunicate:SOCKet<inst>:VRESource

.. code-block:: python

	SYSTem:COMMunicate:SOCKet<inst>:VRESource



.. autoclass:: RsCmwBase.Implementations.System.Communicate.Socket.Vresource.VresourceCls
	:members:
	:undoc-members:
	:noindex: