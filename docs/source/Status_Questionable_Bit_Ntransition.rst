Ntransition
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: STATus:QUEStionable:BIT<bitno>:NTRansition

.. code-block:: python

	STATus:QUEStionable:BIT<bitno>:NTRansition



.. autoclass:: RsCmwBase.Implementations.Status.Questionable.Bit.Ntransition.NtransitionCls
	:members:
	:undoc-members:
	:noindex: