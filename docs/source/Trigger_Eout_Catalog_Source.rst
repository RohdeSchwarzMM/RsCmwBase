Source
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:BASE:EOUT<n>:CATalog:SOURce

.. code-block:: python

	TRIGger:BASE:EOUT<n>:CATalog:SOURce



.. autoclass:: RsCmwBase.Implementations.Trigger.Eout.Catalog.Source.SourceCls
	:members:
	:undoc-members:
	:noindex: