Ipc
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALibration:BASE:IPC:RESult
	single: CALibration:BASE:IPC:VALues
	single: CALibration:BASE:IPC:LOG

.. code-block:: python

	CALibration:BASE:IPC:RESult
	CALibration:BASE:IPC:VALues
	CALibration:BASE:IPC:LOG



.. autoclass:: RsCmwBase.Implementations.Calibration.Ipc.IpcCls
	:members:
	:undoc-members:
	:noindex: