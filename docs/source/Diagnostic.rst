Diagnostic
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:SDBM

.. code-block:: python

	DIAGnostic:SDBM



.. autoclass:: RsCmwBase.Implementations.Diagnostic.DiagnosticCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.diagnostic.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Diagnostic_Access.rst
	Diagnostic_BgInfo.rst
	Diagnostic_Cmw.rst
	Diagnostic_Compass.rst
	Diagnostic_Eeprom.rst
	Diagnostic_Error.rst
	Diagnostic_FootPrint.rst
	Diagnostic_Help.rst
	Diagnostic_Instrument.rst
	Diagnostic_Kremote.rst
	Diagnostic_Log.rst
	Diagnostic_Pias.rst
	Diagnostic_Product.rst
	Diagnostic_Record.rst
	Diagnostic_Routing.rst
	Diagnostic_SingleCmw.rst
	Diagnostic_Status.rst