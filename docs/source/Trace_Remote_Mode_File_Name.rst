Name
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRACe:REMote:MODE:FILE<instrument>:NAME

.. code-block:: python

	TRACe:REMote:MODE:FILE<instrument>:NAME



.. autoclass:: RsCmwBase.Implementations.Trace.Remote.Mode.File.Name.NameCls
	:members:
	:undoc-members:
	:noindex: