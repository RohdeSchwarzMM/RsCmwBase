Local
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:DATE:LOCal

.. code-block:: python

	SYSTem:DATE:LOCal



.. autoclass:: RsCmwBase.Implementations.System.Date.Local.LocalCls
	:members:
	:undoc-members:
	:noindex: