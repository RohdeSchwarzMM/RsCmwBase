Correction
----------------------------------------





.. autoclass:: RsCmwBase.Implementations.Catalog.Correction.CorrectionCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.catalog.correction.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Catalog_Correction_IfEqualizer.rst