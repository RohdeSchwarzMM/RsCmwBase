Dns
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SYSTem:COMMunicate:NET:DNS:ENABle
	single: SYSTem:COMMunicate:NET:DNS

.. code-block:: python

	SYSTem:COMMunicate:NET:DNS:ENABle
	SYSTem:COMMunicate:NET:DNS



.. autoclass:: RsCmwBase.Implementations.System.Communicate.Net.Dns.DnsCls
	:members:
	:undoc-members:
	:noindex: