Properties
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:FOOTprint:ELEMent:PROPerties

.. code-block:: python

	DIAGnostic:FOOTprint:ELEMent:PROPerties



.. autoclass:: RsCmwBase.Implementations.Diagnostic.FootPrint.Element.Properties.PropertiesCls
	:members:
	:undoc-members:
	:noindex: