Device
----------------------------------------





.. autoclass:: RsCmwBase.Implementations.System.Cmw.Device.DeviceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.system.cmw.device.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	System_Cmw_Device_Id.rst