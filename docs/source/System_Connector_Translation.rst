Translation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:CONNector:TRANslation

.. code-block:: python

	SYSTem:CONNector:TRANslation



.. autoclass:: RsCmwBase.Implementations.System.Connector.Translation.TranslationCls
	:members:
	:undoc-members:
	:noindex: