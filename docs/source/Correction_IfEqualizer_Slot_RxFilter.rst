RxFilter
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BASE:CORRection:IFEQualizer:SLOT<Slot>:RXFilter

.. code-block:: python

	FETCh:BASE:CORRection:IFEQualizer:SLOT<Slot>:RXFilter



.. autoclass:: RsCmwBase.Implementations.Correction.IfEqualizer.Slot.RxFilter.RxFilterCls
	:members:
	:undoc-members:
	:noindex: