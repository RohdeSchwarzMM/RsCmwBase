Li
----------------------------------------





.. autoclass:: RsCmwBase.Implementations.Diagnostic.FootPrint.Li.LiCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.diagnostic.footPrint.li.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Diagnostic_FootPrint_Li_Usecases.rst