Select
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: INSTrument[:SELect]

.. code-block:: python

	INSTrument[:SELect]



.. autoclass:: RsCmwBase.Implementations.Instrument.Select.SelectCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.instrument.select.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Instrument_Select_Dstrategy.rst