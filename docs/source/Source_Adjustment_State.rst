State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SOURce:BASE:ADJustment:STATe

.. code-block:: python

	SOURce:BASE:ADJustment:STATe



.. autoclass:: RsCmwBase.Implementations.Source.Adjustment.State.StateCls
	:members:
	:undoc-members:
	:noindex: