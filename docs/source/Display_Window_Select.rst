Select
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DISPlay[:WINDow<1-n>]:SELect

.. code-block:: python

	DISPlay[:WINDow<1-n>]:SELect



.. autoclass:: RsCmwBase.Implementations.Display.Window.Select.SelectCls
	:members:
	:undoc-members:
	:noindex: