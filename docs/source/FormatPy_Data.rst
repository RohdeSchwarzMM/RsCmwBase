Data
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FORMat:BASE[:DATA]

.. code-block:: python

	FORMat:BASE[:DATA]



.. autoclass:: RsCmwBase.Implementations.FormatPy.Data.DataCls
	:members:
	:undoc-members:
	:noindex: