RxFilter
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CATalog:BASE:CORRection:IFEQualizer:SLOT<Slot>:RXFilter

.. code-block:: python

	CATalog:BASE:CORRection:IFEQualizer:SLOT<Slot>:RXFilter



.. autoclass:: RsCmwBase.Implementations.Catalog.Correction.IfEqualizer.Slot.RxFilter.RxFilterCls
	:members:
	:undoc-members:
	:noindex: