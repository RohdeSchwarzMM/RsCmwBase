Macro
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: MMEMory:LOAD:MACRo

.. code-block:: python

	MMEMory:LOAD:MACRo



.. autoclass:: RsCmwBase.Implementations.MassMemory.Load.Macro.MacroCls
	:members:
	:undoc-members:
	:noindex: