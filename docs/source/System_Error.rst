Error
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SYSTem:ERRor:ALL
	single: SYSTem:ERRor:COUNt

.. code-block:: python

	SYSTem:ERRor:ALL
	SYSTem:ERRor:COUNt



.. autoclass:: RsCmwBase.Implementations.System.Error.ErrorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.system.error.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	System_Error_Code.rst