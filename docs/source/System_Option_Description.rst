Description
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:BASE:OPTion:DESCription

.. code-block:: python

	SYSTem:BASE:OPTion:DESCription



.. autoclass:: RsCmwBase.Implementations.System.Option.Description.DescriptionCls
	:members:
	:undoc-members:
	:noindex: