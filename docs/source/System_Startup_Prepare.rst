Prepare
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:STARtup:PREPare:FDEFault

.. code-block:: python

	SYSTem:STARtup:PREPare:FDEFault



.. autoclass:: RsCmwBase.Implementations.System.Startup.Prepare.PrepareCls
	:members:
	:undoc-members:
	:noindex: