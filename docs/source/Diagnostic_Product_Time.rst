Time
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:PRODuct:TIME:OPERating

.. code-block:: python

	DIAGnostic:PRODuct:TIME:OPERating



.. autoclass:: RsCmwBase.Implementations.Diagnostic.Product.Time.TimeCls
	:members:
	:undoc-members:
	:noindex: