Off
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: STATus:MEASurement:CONDition:OFF

.. code-block:: python

	STATus:MEASurement:CONDition:OFF



.. autoclass:: RsCmwBase.Implementations.Status.Measurement.Condition.Off.OffCls
	:members:
	:undoc-members:
	:noindex: