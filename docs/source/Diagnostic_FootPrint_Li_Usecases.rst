Usecases
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:FOOTprint:LI:USECases

.. code-block:: python

	DIAGnostic:FOOTprint:LI:USECases



.. autoclass:: RsCmwBase.Implementations.Diagnostic.FootPrint.Li.Usecases.UsecasesCls
	:members:
	:undoc-members:
	:noindex: