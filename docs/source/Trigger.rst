Trigger
----------------------------------------





.. autoclass:: RsCmwBase.Implementations.Trigger.TriggerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Eout.rst
	Trigger_ExtA.rst
	Trigger_ExtB.rst
	Trigger_Uinitiated.rst