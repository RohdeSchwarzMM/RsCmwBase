Uinitiated<Trigger>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Trg1 .. Trg4
	rc = driver.trigger.uinitiated.repcap_trigger_get()
	driver.trigger.uinitiated.repcap_trigger_set(repcap.Trigger.Trg1)





.. autoclass:: RsCmwBase.Implementations.Trigger.Uinitiated.UinitiatedCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.uinitiated.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Uinitiated_Execute.rst