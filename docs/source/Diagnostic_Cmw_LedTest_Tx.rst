Tx
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:CMW<variant>:LEDTest:TX

.. code-block:: python

	DIAGnostic:CMW<variant>:LEDTest:TX



.. autoclass:: RsCmwBase.Implementations.Diagnostic.Cmw.LedTest.Tx.TxCls
	:members:
	:undoc-members:
	:noindex: