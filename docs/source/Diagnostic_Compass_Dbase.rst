Dbase
----------------------------------------





.. autoclass:: RsCmwBase.Implementations.Diagnostic.Compass.Dbase.DbaseCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.diagnostic.compass.dbase.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Diagnostic_Compass_Dbase_Rlogging.rst
	Diagnostic_Compass_Dbase_TaLogging.rst