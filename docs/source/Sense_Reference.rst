Reference
----------------------------------------





.. autoclass:: RsCmwBase.Implementations.Sense.Reference.ReferenceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.reference.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Reference_Frequency.rst