GotoLocal
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: *GTL

.. code-block:: python

	*GTL



.. autoclass:: RsCmwBase.Implementations.GotoLocal.GotoLocalCls
	:members:
	:undoc-members:
	:noindex: