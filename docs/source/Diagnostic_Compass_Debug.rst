Debug
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:COMPass:DEBug:MODE

.. code-block:: python

	DIAGnostic:COMPass:DEBug:MODE



.. autoclass:: RsCmwBase.Implementations.Diagnostic.Compass.Debug.DebugCls
	:members:
	:undoc-members:
	:noindex: