Uncorrected
----------------------------------------





.. autoclass:: RsCmwBase.Implementations.Correction.IfEqualizer.Trace.Gdelay.Uncorrected.UncorrectedCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.correction.ifEqualizer.trace.gdelay.uncorrected.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Correction_IfEqualizer_Trace_Gdelay_Uncorrected_Slot.rst