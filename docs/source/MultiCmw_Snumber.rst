Snumber
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BASE:MCMW:SNUMber

.. code-block:: python

	FETCh:BASE:MCMW:SNUMber



.. autoclass:: RsCmwBase.Implementations.MultiCmw.Snumber.SnumberCls
	:members:
	:undoc-members:
	:noindex: