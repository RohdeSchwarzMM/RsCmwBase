TxFilter
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BASE:CORRection:IFEQualizer:SLOT<Slot>:TXFilter

.. code-block:: python

	FETCh:BASE:CORRection:IFEQualizer:SLOT<Slot>:TXFilter



.. autoclass:: RsCmwBase.Implementations.Correction.IfEqualizer.Slot.TxFilter.TxFilterCls
	:members:
	:undoc-members:
	:noindex: