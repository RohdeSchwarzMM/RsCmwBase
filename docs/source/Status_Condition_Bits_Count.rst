Count
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: STATus:CONDition:BITS:COUNt

.. code-block:: python

	STATus:CONDition:BITS:COUNt



.. autoclass:: RsCmwBase.Implementations.Status.Condition.Bits.Count.CountCls
	:members:
	:undoc-members:
	:noindex: