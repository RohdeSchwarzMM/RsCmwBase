Offset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:BASE:REFerence:DC:OFFSet:ENABle

.. code-block:: python

	SYSTem:BASE:REFerence:DC:OFFSet:ENABle



.. autoclass:: RsCmwBase.Implementations.System.Reference.Dc.Offset.OffsetCls
	:members:
	:undoc-members:
	:noindex: