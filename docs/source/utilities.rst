RsCmwBase Utilities
==========================

.. _Utilities:

.. autoclass:: RsCmwBase.CustomFiles.utilities.Utilities()
   :members:
   :undoc-members:
   :special-members: enable_properties
   :noindex:
   :member-order: bysource
