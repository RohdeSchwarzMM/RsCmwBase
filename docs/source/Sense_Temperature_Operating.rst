Operating
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:BASE:TEMPerature:OPERating:INTernal

.. code-block:: python

	SENSe:BASE:TEMPerature:OPERating:INTernal



.. autoclass:: RsCmwBase.Implementations.Sense.Temperature.Operating.OperatingCls
	:members:
	:undoc-members:
	:noindex: