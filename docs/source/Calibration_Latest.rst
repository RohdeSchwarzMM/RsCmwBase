Latest
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CALibration:BASE:LATest

.. code-block:: python

	CALibration:BASE:LATest



.. autoclass:: RsCmwBase.Implementations.Calibration.Latest.LatestCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.calibration.latest.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Calibration_Latest_Specific.rst