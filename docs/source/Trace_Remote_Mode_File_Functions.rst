Functions
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRACe:REMote:MODE:FILE<instrument>:FUNCtions

.. code-block:: python

	TRACe:REMote:MODE:FILE<instrument>:FUNCtions



.. autoclass:: RsCmwBase.Implementations.Trace.Remote.Mode.File.Functions.FunctionsCls
	:members:
	:undoc-members:
	:noindex: