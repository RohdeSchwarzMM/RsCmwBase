Device
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:CMWS:DEVice:ID

.. code-block:: python

	SYSTem:CMWS:DEVice:ID



.. autoclass:: RsCmwBase.Implementations.System.SingleCmw.Device.DeviceCls
	:members:
	:undoc-members:
	:noindex: