Kremote
----------------------------------------





.. autoclass:: RsCmwBase.Implementations.Diagnostic.Kremote.KremoteCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.diagnostic.kremote.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Diagnostic_Kremote_Tmonitor.rst