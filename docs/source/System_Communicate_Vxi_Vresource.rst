Vresource
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:COMMunicate:VXI<inst>:VRESource

.. code-block:: python

	SYSTem:COMMunicate:VXI<inst>:VRESource



.. autoclass:: RsCmwBase.Implementations.System.Communicate.Vxi.Vresource.VresourceCls
	:members:
	:undoc-members:
	:noindex: