FootPrint
----------------------------------------





.. autoclass:: RsCmwBase.Implementations.Diagnostic.FootPrint.FootPrintCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.diagnostic.footPrint.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Diagnostic_FootPrint_Element.rst
	Diagnostic_FootPrint_Li.rst
	Diagnostic_FootPrint_UseCase.rst