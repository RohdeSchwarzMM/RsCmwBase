Ids
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:FOOTprint:ELEMent:CONNection:TARGet:IDS

.. code-block:: python

	DIAGnostic:FOOTprint:ELEMent:CONNection:TARGet:IDS



.. autoclass:: RsCmwBase.Implementations.Diagnostic.FootPrint.Element.Connection.Target.Ids.IdsCls
	:members:
	:undoc-members:
	:noindex: