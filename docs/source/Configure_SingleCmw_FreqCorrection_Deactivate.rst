Deactivate
----------------------------------------





.. autoclass:: RsCmwBase.Implementations.Configure.SingleCmw.FreqCorrection.Deactivate.DeactivateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.singleCmw.freqCorrection.deactivate.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_SingleCmw_FreqCorrection_Deactivate_Rx.rst
	Configure_SingleCmw_FreqCorrection_Deactivate_Tx.rst