Push
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:ERRor:QUEue:PUSH

.. code-block:: python

	DIAGnostic:ERRor:QUEue:PUSH



.. autoclass:: RsCmwBase.Implementations.Diagnostic.Error.Queue.Push.PushCls
	:members:
	:undoc-members:
	:noindex: