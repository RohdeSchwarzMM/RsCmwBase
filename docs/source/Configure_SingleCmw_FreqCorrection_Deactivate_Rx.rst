Rx
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:CMWS:FDCorrection:DEACtivate:RX

.. code-block:: python

	CONFigure:CMWS:FDCorrection:DEACtivate:RX



.. autoclass:: RsCmwBase.Implementations.Configure.SingleCmw.FreqCorrection.Deactivate.Rx.RxCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.singleCmw.freqCorrection.deactivate.rx.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_SingleCmw_FreqCorrection_Deactivate_Rx_All.rst