File
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SYSTem:RECord:MACRo:FILE:STARt
	single: SYSTem:RECord:MACRo:FILE:STOP

.. code-block:: python

	SYSTem:RECord:MACRo:FILE:STARt
	SYSTem:RECord:MACRo:FILE:STOP



.. autoclass:: RsCmwBase.Implementations.System.Record.Macro.File.FileCls
	:members:
	:undoc-members:
	:noindex: