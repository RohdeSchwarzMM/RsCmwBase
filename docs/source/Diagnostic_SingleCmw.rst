SingleCmw
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:CMWS:LEDTest

.. code-block:: python

	DIAGnostic:CMWS:LEDTest



.. autoclass:: RsCmwBase.Implementations.Diagnostic.SingleCmw.SingleCmwCls
	:members:
	:undoc-members:
	:noindex: