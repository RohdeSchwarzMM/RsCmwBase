Count
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: STATus:EVENt:BITS:COUNt

.. code-block:: python

	STATus:EVENt:BITS:COUNt



.. autoclass:: RsCmwBase.Implementations.Status.Event.Bits.Count.CountCls
	:members:
	:undoc-members:
	:noindex: