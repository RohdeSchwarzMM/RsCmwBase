State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: MMEMory:LOAD:STATe

.. code-block:: python

	MMEMory:LOAD:STATe



.. autoclass:: RsCmwBase.Implementations.MassMemory.Load.State.StateCls
	:members:
	:undoc-members:
	:noindex: