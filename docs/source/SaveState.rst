SaveState
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: *SAV

.. code-block:: python

	*SAV



.. autoclass:: RsCmwBase.Implementations.SaveState.SaveStateCls
	:members:
	:undoc-members:
	:noindex: