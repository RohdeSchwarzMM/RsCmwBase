Connector
----------------------------------------





.. autoclass:: RsCmwBase.Implementations.System.Connector.ConnectorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.system.connector.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	System_Connector_Translation.rst