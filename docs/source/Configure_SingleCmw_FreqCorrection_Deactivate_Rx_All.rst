All
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:CMWS:FDCorrection:DEACtivate:RX:ALL

.. code-block:: python

	CONFigure:CMWS:FDCorrection:DEACtivate:RX:ALL



.. autoclass:: RsCmwBase.Implementations.Configure.SingleCmw.FreqCorrection.Deactivate.Rx.All.AllCls
	:members:
	:undoc-members:
	:noindex: