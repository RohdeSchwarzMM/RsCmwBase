HardCopy
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: HCOPy:DATA
	single: HCOPy:FILE

.. code-block:: python

	HCOPy:DATA
	HCOPy:FILE



.. autoclass:: RsCmwBase.Implementations.HardCopy.HardCopyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.hardCopy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	HardCopy_Device.rst
	HardCopy_Interior.rst