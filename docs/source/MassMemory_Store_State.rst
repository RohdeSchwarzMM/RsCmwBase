State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: MMEMory:STORe:STATe

.. code-block:: python

	MMEMory:STORe:STATe



.. autoclass:: RsCmwBase.Implementations.MassMemory.Store.State.StateCls
	:members:
	:undoc-members:
	:noindex: