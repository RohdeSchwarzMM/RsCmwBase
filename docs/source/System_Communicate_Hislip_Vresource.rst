Vresource
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:COMMunicate:HISLip<inst>:VRESource

.. code-block:: python

	SYSTem:COMMunicate:HISLip<inst>:VRESource



.. autoclass:: RsCmwBase.Implementations.System.Communicate.Hislip.Vresource.VresourceCls
	:members:
	:undoc-members:
	:noindex: