Scenario
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:ACCess:SCENario

.. code-block:: python

	DIAGnostic:ACCess:SCENario



.. autoclass:: RsCmwBase.Implementations.Diagnostic.Access.Scenario.ScenarioCls
	:members:
	:undoc-members:
	:noindex: