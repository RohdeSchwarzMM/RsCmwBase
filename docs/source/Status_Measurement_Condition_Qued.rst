Qued
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: STATus:MEASurement:CONDition:QUED

.. code-block:: python

	STATus:MEASurement:CONDition:QUED



.. autoclass:: RsCmwBase.Implementations.Status.Measurement.Condition.Qued.QuedCls
	:members:
	:undoc-members:
	:noindex: