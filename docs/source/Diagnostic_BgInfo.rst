BgInfo
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:BGINfo:CATalog

.. code-block:: python

	DIAGnostic:BGINfo:CATalog



.. autoclass:: RsCmwBase.Implementations.Diagnostic.BgInfo.BgInfoCls
	:members:
	:undoc-members:
	:noindex: