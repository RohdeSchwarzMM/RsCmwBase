Store
----------------------------------------





.. autoclass:: RsCmwBase.Implementations.MassMemory.Store.StoreCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.massMemory.store.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MassMemory_Store_Item.rst
	MassMemory_Store_Macro.rst
	MassMemory_Store_State.rst