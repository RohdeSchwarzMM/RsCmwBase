Display
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DISPlay:FORMat

.. code-block:: python

	DISPlay:FORMat



.. autoclass:: RsCmwBase.Implementations.Display.DisplayCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.display.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Display_Window.rst