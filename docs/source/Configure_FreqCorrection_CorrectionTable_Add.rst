Add
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BASE:FDCorrection:CTABle:ADD

.. code-block:: python

	CONFigure:BASE:FDCorrection:CTABle:ADD



.. autoclass:: RsCmwBase.Implementations.Configure.FreqCorrection.CorrectionTable.Add.AddCls
	:members:
	:undoc-members:
	:noindex: