Instrument
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: DIAGnostic:INSTrument:LOAD
	single: DIAGnostic:INSTrument:UNLoad

.. code-block:: python

	DIAGnostic:INSTrument:LOAD
	DIAGnostic:INSTrument:UNLoad



.. autoclass:: RsCmwBase.Implementations.Diagnostic.Instrument.InstrumentCls
	:members:
	:undoc-members:
	:noindex: