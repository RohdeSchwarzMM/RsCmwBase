Instrument
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: INSTrument:NSELect

.. code-block:: python

	INSTrument:NSELect



.. autoclass:: RsCmwBase.Implementations.Instrument.InstrumentCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.instrument.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Instrument_Display.rst
	Instrument_Select.rst