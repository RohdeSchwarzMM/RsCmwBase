Setup
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:ROUTing:EXPert:SETup

.. code-block:: python

	DIAGnostic:ROUTing:EXPert:SETup



.. autoclass:: RsCmwBase.Implementations.Diagnostic.Routing.Expert.Setup.SetupCls
	:members:
	:undoc-members:
	:noindex: