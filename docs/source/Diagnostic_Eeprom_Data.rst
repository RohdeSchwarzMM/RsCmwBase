Data
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:EEPRom:DATA

.. code-block:: python

	DIAGnostic:EEPRom:DATA



.. autoclass:: RsCmwBase.Implementations.Diagnostic.Eeprom.Data.DataCls
	:members:
	:undoc-members:
	:noindex: