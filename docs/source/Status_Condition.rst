Condition
----------------------------------------





.. autoclass:: RsCmwBase.Implementations.Status.Condition.ConditionCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.status.condition.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Status_Condition_Bits.rst