DeviceFootprint
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:DFPRint

.. code-block:: python

	SYSTem:DFPRint



.. autoclass:: RsCmwBase.Implementations.System.DeviceFootprint.DeviceFootprintCls
	:members:
	:undoc-members:
	:noindex: