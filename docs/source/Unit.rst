Unit
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: UNIT:CONDuctance
	single: UNIT:CHARge
	single: UNIT:CAPacity
	single: UNIT:ENERgy
	single: UNIT:FREQuency
	single: UNIT:RESistor
	single: UNIT:VOLTage
	single: UNIT:ANGLe
	single: UNIT:LENGth
	single: UNIT:CURRent
	single: UNIT:POWer
	single: UNIT:TEMPerature
	single: UNIT:TIME

.. code-block:: python

	UNIT:CONDuctance
	UNIT:CHARge
	UNIT:CAPacity
	UNIT:ENERgy
	UNIT:FREQuency
	UNIT:RESistor
	UNIT:VOLTage
	UNIT:ANGLe
	UNIT:LENGth
	UNIT:CURRent
	UNIT:POWer
	UNIT:TEMPerature
	UNIT:TIME



.. autoclass:: RsCmwBase.Implementations.Unit.UnitCls
	:members:
	:undoc-members:
	:noindex: