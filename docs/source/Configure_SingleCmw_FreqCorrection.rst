FreqCorrection
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:CMWS:FDCorrection:DEACtivate:ALL

.. code-block:: python

	CONFigure:CMWS:FDCorrection:DEACtivate:ALL



.. autoclass:: RsCmwBase.Implementations.Configure.SingleCmw.FreqCorrection.FreqCorrectionCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.singleCmw.freqCorrection.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_SingleCmw_FreqCorrection_Activate.rst
	Configure_SingleCmw_FreqCorrection_Deactivate.rst
	Configure_SingleCmw_FreqCorrection_Usage.rst