Define
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:SPOint:DEFine

.. code-block:: python

	CONFigure:SPOint:DEFine



.. autoclass:: RsCmwBase.Implementations.Configure.Spoint.Define.DefineCls
	:members:
	:undoc-members:
	:noindex: