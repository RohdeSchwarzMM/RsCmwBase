Multiple
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:PIAS:CONNect:MULTiple

.. code-block:: python

	DIAGnostic:PIAS:CONNect:MULTiple



.. autoclass:: RsCmwBase.Implementations.Diagnostic.Pias.Connect.Multiple.MultipleCls
	:members:
	:undoc-members:
	:noindex: