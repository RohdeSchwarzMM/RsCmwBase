Cenable
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SYSTem:BASE:PASSword[:CENable]:STATe
	single: SYSTem:BASE:PASSword[:CENable]

.. code-block:: python

	SYSTem:BASE:PASSword[:CENable]:STATe
	SYSTem:BASE:PASSword[:CENable]



.. autoclass:: RsCmwBase.Implementations.System.Password.Cenable.CenableCls
	:members:
	:undoc-members:
	:noindex: