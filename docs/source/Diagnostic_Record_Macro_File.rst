File
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: DIAGnostic:RECord:MACRo:FILE:SIZE
	single: DIAGnostic:RECord:MACRo:FILE:FILTer

.. code-block:: python

	DIAGnostic:RECord:MACRo:FILE:SIZE
	DIAGnostic:RECord:MACRo:FILE:FILTer



.. autoclass:: RsCmwBase.Implementations.Diagnostic.Record.Macro.File.FileCls
	:members:
	:undoc-members:
	:noindex: