Port
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:COMMunicate:SOCKet<inst>:PORT

.. code-block:: python

	SYSTem:COMMunicate:SOCKet<inst>:PORT



.. autoclass:: RsCmwBase.Implementations.System.Communicate.Socket.Port.PortCls
	:members:
	:undoc-members:
	:noindex: