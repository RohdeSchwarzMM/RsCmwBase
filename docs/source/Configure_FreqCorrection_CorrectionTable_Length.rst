Length
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BASE:FDCorrection:CTABle:LENGth

.. code-block:: python

	CONFigure:BASE:FDCorrection:CTABle:LENGth



.. autoclass:: RsCmwBase.Implementations.Configure.FreqCorrection.CorrectionTable.Length.LengthCls
	:members:
	:undoc-members:
	:noindex: