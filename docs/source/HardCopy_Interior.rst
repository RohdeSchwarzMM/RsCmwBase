Interior
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: HCOPy:INTerior:DATA
	single: HCOPy:INTerior:FILE

.. code-block:: python

	HCOPy:INTerior:DATA
	HCOPy:INTerior:FILE



.. autoclass:: RsCmwBase.Implementations.HardCopy.Interior.InteriorCls
	:members:
	:undoc-members:
	:noindex: