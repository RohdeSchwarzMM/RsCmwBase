Execute
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:BASE:UINitiated<n>:EXECute

.. code-block:: python

	TRIGger:BASE:UINitiated<n>:EXECute



.. autoclass:: RsCmwBase.Implementations.Trigger.Uinitiated.Execute.ExecuteCls
	:members:
	:undoc-members:
	:noindex: