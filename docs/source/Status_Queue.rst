Queue
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: STATus:QUEue[:NEXT]

.. code-block:: python

	STATus:QUEue[:NEXT]



.. autoclass:: RsCmwBase.Implementations.Status.Queue.QueueCls
	:members:
	:undoc-members:
	:noindex: