Communicate
----------------------------------------





.. autoclass:: RsCmwBase.Implementations.System.Communicate.CommunicateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.system.communicate.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	System_Communicate_Gpib.rst
	System_Communicate_Hislip.rst
	System_Communicate_Net.rst
	System_Communicate_Rsib.rst
	System_Communicate_Socket.rst
	System_Communicate_Usb.rst
	System_Communicate_Vxi.rst