Catalog
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BASE:FDCorrection:CTABle:CATalog

.. code-block:: python

	CONFigure:BASE:FDCorrection:CTABle:CATalog



.. autoclass:: RsCmwBase.Implementations.Configure.FreqCorrection.CorrectionTable.Catalog.CatalogCls
	:members:
	:undoc-members:
	:noindex: