Define
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:MUTex:DEFine

.. code-block:: python

	CONFigure:MUTex:DEFine



.. autoclass:: RsCmwBase.Implementations.Configure.Mutex.Define.DefineCls
	:members:
	:undoc-members:
	:noindex: