ListPy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:BASE:OPTion:LIST

.. code-block:: python

	SYSTem:BASE:OPTion:LIST



.. autoclass:: RsCmwBase.Implementations.System.Option.ListPy.ListPyCls
	:members:
	:undoc-members:
	:noindex: