Rpc
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRACe:REMote:MODE:FILE<instrument>:RPC

.. code-block:: python

	TRACe:REMote:MODE:FILE<instrument>:RPC



.. autoclass:: RsCmwBase.Implementations.Trace.Remote.Mode.File.Rpc.RpcCls
	:members:
	:undoc-members:
	:noindex: