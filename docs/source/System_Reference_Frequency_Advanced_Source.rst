Source
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:BASE:REFerence:FREQuency<n>:ADVanced:SOURce

.. code-block:: python

	SYSTem:BASE:REFerence:FREQuency<n>:ADVanced:SOURce



.. autoclass:: RsCmwBase.Implementations.System.Reference.Frequency.Advanced.Source.SourceCls
	:members:
	:undoc-members:
	:noindex: