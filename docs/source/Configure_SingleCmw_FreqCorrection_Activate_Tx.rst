Tx
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:CMWS:FDCorrection:ACTivate:TX

.. code-block:: python

	CONFigure:CMWS:FDCorrection:ACTivate:TX



.. autoclass:: RsCmwBase.Implementations.Configure.SingleCmw.FreqCorrection.Activate.Tx.TxCls
	:members:
	:undoc-members:
	:noindex: