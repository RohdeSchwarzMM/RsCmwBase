Frequency
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:BASE:REFerence:FREQuency:LOCKed

.. code-block:: python

	SENSe:BASE:REFerence:FREQuency:LOCKed



.. autoclass:: RsCmwBase.Implementations.Sense.Reference.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: