Create
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BASE:FDCorrection:CTABle:CREate

.. code-block:: python

	CONFigure:BASE:FDCorrection:CTABle:CREate



.. autoclass:: RsCmwBase.Implementations.Configure.FreqCorrection.CorrectionTable.Create.CreateCls
	:members:
	:undoc-members:
	:noindex: