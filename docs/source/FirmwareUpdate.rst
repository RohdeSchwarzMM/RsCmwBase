FirmwareUpdate
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:FWUPdate:VERSions

.. code-block:: python

	FETCh:FWUPdate:VERSions



.. autoclass:: RsCmwBase.Implementations.FirmwareUpdate.FirmwareUpdateCls
	:members:
	:undoc-members:
	:noindex: