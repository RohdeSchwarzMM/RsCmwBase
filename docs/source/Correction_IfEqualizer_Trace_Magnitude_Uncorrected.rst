Uncorrected
----------------------------------------





.. autoclass:: RsCmwBase.Implementations.Correction.IfEqualizer.Trace.Magnitude.Uncorrected.UncorrectedCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.correction.ifEqualizer.trace.magnitude.uncorrected.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Correction_IfEqualizer_Trace_Magnitude_Uncorrected_Slot.rst