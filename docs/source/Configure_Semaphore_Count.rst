Count
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:SEMaphore:COUNt

.. code-block:: python

	CONFigure:SEMaphore:COUNt



.. autoclass:: RsCmwBase.Implementations.Configure.Semaphore.Count.CountCls
	:members:
	:undoc-members:
	:noindex: