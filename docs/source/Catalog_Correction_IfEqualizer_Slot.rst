Slot<Slot>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr4
	rc = driver.catalog.correction.ifEqualizer.slot.repcap_slot_get()
	driver.catalog.correction.ifEqualizer.slot.repcap_slot_set(repcap.Slot.Nr1)





.. autoclass:: RsCmwBase.Implementations.Catalog.Correction.IfEqualizer.Slot.SlotCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.catalog.correction.ifEqualizer.slot.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Catalog_Correction_IfEqualizer_Slot_RxFilter.rst
	Catalog_Correction_IfEqualizer_Slot_TxFilter.rst