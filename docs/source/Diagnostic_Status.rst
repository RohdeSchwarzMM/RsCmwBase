Status
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:STATus:OPC

.. code-block:: python

	DIAGnostic:STATus:OPC



.. autoclass:: RsCmwBase.Implementations.Diagnostic.Status.StatusCls
	:members:
	:undoc-members:
	:noindex: