MultiCmw
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:BASE:MCMW:REARrange

.. code-block:: python

	CONFigure:BASE:MCMW:REARrange



.. autoclass:: RsCmwBase.Implementations.Configure.MultiCmw.MultiCmwCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiCmw.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiCmw_Identify.rst