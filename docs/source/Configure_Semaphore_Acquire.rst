Acquire
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:SEMaphore:ACQuire

.. code-block:: python

	CONFigure:SEMaphore:ACQuire



.. autoclass:: RsCmwBase.Implementations.Configure.Semaphore.Acquire.AcquireCls
	:members:
	:undoc-members:
	:noindex: