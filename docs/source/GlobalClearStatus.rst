GlobalClearStatus
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: *GCLS

.. code-block:: python

	*GCLS



.. autoclass:: RsCmwBase.Implementations.GlobalClearStatus.GlobalClearStatusCls
	:members:
	:undoc-members:
	:noindex: