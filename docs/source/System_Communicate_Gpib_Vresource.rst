Vresource
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:COMMunicate:GPIB<inst>:VRESource

.. code-block:: python

	SYSTem:COMMunicate:GPIB<inst>:VRESource



.. autoclass:: RsCmwBase.Implementations.System.Communicate.Gpib.Vresource.VresourceCls
	:members:
	:undoc-members:
	:noindex: