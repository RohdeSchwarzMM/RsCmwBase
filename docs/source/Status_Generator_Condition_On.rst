On
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: STATus:GENerator:CONDition:ON

.. code-block:: python

	STATus:GENerator:CONDition:ON



.. autoclass:: RsCmwBase.Implementations.Status.Generator.Condition.On.OnCls
	:members:
	:undoc-members:
	:noindex: