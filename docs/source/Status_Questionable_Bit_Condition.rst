Condition
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: STATus:QUEStionable:BIT<bitno>:CONDition

.. code-block:: python

	STATus:QUEStionable:BIT<bitno>:CONDition



.. autoclass:: RsCmwBase.Implementations.Status.Questionable.Bit.Condition.ConditionCls
	:members:
	:undoc-members:
	:noindex: