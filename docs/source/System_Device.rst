Device
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SYSTem:BASE:DEVice:SUBinst
	single: SYSTem:BASE:DEVice:COUNt
	single: SYSTem:BASE:DEVice:RESet
	single: SYSTem:BASE:DEVice:MSCont
	single: SYSTem:BASE:DEVice:MSCCount
	single: SYSTem:DEVice:ID

.. code-block:: python

	SYSTem:BASE:DEVice:SUBinst
	SYSTem:BASE:DEVice:COUNt
	SYSTem:BASE:DEVice:RESet
	SYSTem:BASE:DEVice:MSCont
	SYSTem:BASE:DEVice:MSCCount
	SYSTem:DEVice:ID



.. autoclass:: RsCmwBase.Implementations.System.Device.DeviceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.system.device.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	System_Device_License.rst
	System_Device_Setup.rst