Protocol
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic:COMPass:DBASe:TALogging:PROTocol

.. code-block:: python

	DIAGnostic:COMPass:DBASe:TALogging:PROTocol



.. autoclass:: RsCmwBase.Implementations.Diagnostic.Compass.Dbase.TaLogging.Protocol.ProtocolCls
	:members:
	:undoc-members:
	:noindex: