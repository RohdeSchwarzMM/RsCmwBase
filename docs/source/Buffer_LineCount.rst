LineCount
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:BASE:BUFFer:LINecount

.. code-block:: python

	FETCh:BASE:BUFFer:LINecount



.. autoclass:: RsCmwBase.Implementations.Buffer.LineCount.LineCountCls
	:members:
	:undoc-members:
	:noindex: