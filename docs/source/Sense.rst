Sense
----------------------------------------





.. autoclass:: RsCmwBase.Implementations.Sense.SenseCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_FirmwareUpdate.rst
	Sense_IpSet.rst
	Sense_Reference.rst
	Sense_Temperature.rst